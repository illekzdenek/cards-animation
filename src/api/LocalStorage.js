


// To store data
export function setItem(key, value){
    if (typeof window !== "undefined") {
        ////console.log("settingItem")
        let objectValue = {
            data: value,
            type: typeof value
        }
        objectValue = JSON.stringify(objectValue)
        localStorage.setItem(key, objectValue);
    }
}
// To retrieve data
export function getItem(key){ 
    if (typeof window !== "undefined") {
        return JSON.parse(localStorage.getItem(key))
    }

    return null
}
// To clear a specific item
export function removeItem(key){
    if (typeof window !== "undefined") {
        localStorage.removeItem(key);
    }
}
// To clear the whole data stored in localStorage
export function clear(){
    if (typeof window !== "undefined") {
        localStorage.clear();
    }
}
//LocalStorage Debug only
export function writeAll(){
    if (typeof window !== "undefined") {
        let data = []
        for (var i = 0; i < localStorage.length; i++){
            data.push(JSON.parse((localStorage.getItem(localStorage.key(i)))))
        }

        return data
    }
    return null
}