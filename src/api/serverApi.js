import axios from "axios";
import * as restApi from "../constants/apis"
import * as localStorageConstants from "../constants/localStorages"
import {setItem, getItem} from "./LocalStorage"

const requestConfig = {
    validateStatus: function (status) {
        return status < 500; // Resolve only if the status code is less than 500
    }
}

axios.interceptors.request.use(req => {
    // `req` is the Axios request config, so you can modify
    // the `headers`.
    //////console.log(getItem(localStorageConstants.USER))

    if (getItem(localStorageConstants.USER) && getItem(localStorageConstants.USER)!==null){
        req.headers.authorization = "Bearer "+getItem(localStorageConstants.USER).data.accessToken;
    }
    ////console.log(req)
    return req;
});

axios.interceptors.response.use(res => {
    //////console.log(res.status);
    // Important: response interceptors **must** return the response.
    return res;
});

export function postRegister(email, password){
    console.log(email, password)
    axios.post(restApi.REGISTER,{
        email:email,
        password:password
    },requestConfig).then((response) => {
        console.log(response.data)
    })
}

export function postLogin(email, password, callback){
    axios.post(restApi.LOGIN,{
        email:email,
        password:password
    },requestConfig).then((response) => {
        setItem(localStorageConstants.USER, {
            accessToken: response.data.accessToken,
            refreshToken: response.data.refreshToken
        })
        console.log(response.data)
        callback(response.data)
    })
}


export function getNextItem(){
    axios.get(restApi.NEXT_ITEM,requestConfig).then((response) => {
        console.log(response.data)
    })
}

// export function postLogOut(callback){
    
//     if (getItem(localStorageConstants.USER) !== null){
//         axios.post(restApi.LOGOUT,{
//             token:getItem(localStorageConstants.USER).data.refreshToken
//         },requestConfig).then((response) => {
//             //////console.log(response.data)
//             callback()
//         })
//     }
// }

export async function postUserValidation(callback){
    ////console.log(getItem(localStorageConstants.USER))
    if (getItem(localStorageConstants.USER) !== null){
        ////console.log(restApi.USER_VALIDATION)
        axios.post(restApi.USER_VALIDATION, {
            token:getItem(localStorageConstants.USER).data.refreshToken
        },requestConfig).then((response) => {
            console.log(response.data)
            callback(response.data.loggedIn)
        })
    }
}