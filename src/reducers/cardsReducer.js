import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { fetchCardsData } from '../api/mockupApi';
import * as view from "../constants/view"
import { getNextItem } from '../api/serverApi';

const initialState = {
    activeCard: null,
    view: view.CARDS,
    user:{
        logged:false
    },
    items:{},
    status: 'idle',
};

export const fetchDataAsync = createAsyncThunk(
    'cards/fetchCardsData',
    async (carId) => {
        const response = await fetchCardsData(carId);
        return response.data;
    }
);

export const fetchItemsAsync = createAsyncThunk(
    'cards/fetchItemsAsync',
    async () => {
        const response = await getNextItem();
        return response.data;
    }
);

export const cardsSlice = createSlice({
    name: 'cards',
    initialState,
    reducers: {
        //incrementByAmount: (state, action) => {
        //  state.value += action.payload;
        //},
        setView: (state, action) => {
            console.log(action)
            state.view = action.payload;
        },
        login:(state, action) => {
            console.log(action)
            let newUser = action.payload
            newUser.logged = true;
            state.user = newUser;
        }
    },
    extraReducers: (builder) => {
        builder
        .addCase(fetchDataAsync.pending, (state) => {
            state.status = 'loading';
        })
        .addCase(fetchDataAsync.fulfilled, (state, action) => {
            state.status = 'idle';
            state.activeCard = action.payload;
        })
        .addCase(fetchItemsAsync.pending, (state) => {
            state.status = 'loading';
        })
        .addCase(fetchItemsAsync.fulfilled, (state, action) => {
            state.status = 'idle';
            state.items = action.payload;
        });
    },
});

export const { setView, login, incrementByAmount } = cardsSlice.actions;

export const selectCards = (state) => state.cards;


export default cardsSlice.reducer;
