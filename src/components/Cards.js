import React, { useState, useRef } from 'react'
import { useSprings, animated, to as interpolate } from '@react-spring/web'
import { useDrag, useDrop } from '@use-gesture/react'


import { useSelector, useDispatch } from 'react-redux';
import {
    setView,
    fetchDataAsync,
   selectCards,
} from '../reducers/cardsReducer';

import styles from './styles.module.css'
import Galery from './Galery';

import {CheckCircleOutlined, CloseCircleOutlined} from "@ant-design/icons"

const cards = [
    'https://upload.wikimedia.org/wikipedia/en/f/f5/RWS_Tarot_08_Strength.jpg'
]

// These two are just helpers, they curate spring data, values that are later being interpolated into css
const to = (i) => ({
    x: 0,
    y:0,
    //y: i * -4,
    scale: 1,
    //rot: -10 + Math.random() * 20,
    delay: i * 100,
})
const from = (_i) => ({ x: 0, rot: 0, scale: 1, y: -1000 })

const toDetail = (i) => ({
    x: 0,
    y:0,
    scale: 1,
    delay: i * 100,
    height: "0px"
})
const fromDetail = (_i) => ({ x: 0, rot: 0, scale: 1, y: -1000,
    height: "0px" })
// This is being used down there in the view, it interpolates rotation and scale into a css transform
const trans = (r, s) =>
    //`perspective(1500px) rotateX(30deg) rotateY(${r / 10}deg) rotateZ(${r}deg) scale(${s})`
    `perspective(1500px)  scale(${s},1)`

function Deck({okIcon, noIcon}) {
    const detailRef = useRef()
    const crds = useSelector(selectCards);
    const dispatch = useDispatch();
    const [cardState, setCardState] = useState(cards)
console.log(crds)
    //detailAnimations
    const [springsDetail, apiDetail] = useSprings(cardState.length, i => ({ 
        ...toDetail(i),
        from:fromDetail(i)
    }))

    const [gone] = useState(() => new Set()) // The set flags all the cards that are flicked out
    const [springs, api] = useSprings(cardState.length, i => ({
        ...to(i),
        from: from(i),
    })) // Create a bunch of springs using the helpers above
    // Create a gesture, we're interested in down-state, delta (current-pos - click-pos), direction and velocity
    const bind =  useDrag (({ args: [index], active, movement: [mx, my], direction: [xDir, yDir], velocity: [vx, vy] }) =>  {
        //const trigger = vx > 0.2 // If you flick hard enough it should trigger the card to fly out
        const trigger = vx > 2 && vx > vy
        
        if (!active && trigger) gone.add(index) // If button/finger's up and trigger velocity is reached, we flag the card ready to fly out
        api.start(i => {
            if (index !== i) return // We're only interested in changing spring-data for the current spring //pokud je aktivní drag na jedné kartě, nebude se spouštět na dalších
            const isGone = gone.has(index) 
            const x = isGone ? (200 + window.innerWidth) * xDir : active ? mx : 0 // When a card is gone it flys out left or right, otherwise goes back to zero
            const rot = mx / 100 + (isGone ? xDir * 10 * vx : 0) // How much the card tilts, flicking it harder makes it rotate faster
            const scale = active ? 0.9 : 1 // Active cards lift up a bit
            //y
            const detailsTrigger = vy > 2 && vy > vx
            let y = 0
            if (!active && detailsTrigger){
                if (yDir > 0){
                    y = (window.innerHeight-100)
                }
            } else if(active) {
                y = my
            } else {
                y = 0
            }
            const opacityOk = x===0?0:(mx)/300
            const opacityNo = x===0?0:(mx*-1)/300
            okIcon.current.style.opacity = opacityOk
            noIcon.current.style.opacity = opacityNo
            //const y = (!active && detailsTrigger) ? (window.innerHeight-100) * yDir : active ? my : 0


            if (y === 0){
                apiDetail.start(i => {
                    return{
                        x,
                        y,
                        delay: 0,
                        height: "0px"
                    }
                })
            }

            if (!active && !isGone && yDir === 1 && detailsTrigger){
                detailRef.current.style.display = "block"
                detailRef.current.style.background = "aliceblue"
                detailRef.current.style.width = "100vw"
                detailRef.current.style.zIndex = "20"
                apiDetail.start(i => {
                    return{
                        x,
                        y,
                        delay: 0,
                        height: window.innerHeight-100+"px"
                    }
                })
            }
            

            if (isGone && xDir === 1){
                apiDetail.start(i => {
                    return{
                        x,
                        y,
                        delay: 0,
                        height: "0px",
                        display: "none"
                    }
                })
                dispatch(fetchDataAsync(i)).then((e) => {
                    setCardState([...cardState, e.payload.image])
                    okIcon.current.style.opacity = 0
                })
            }
            if (isGone && xDir === -1){
                apiDetail.start(i => {
                    return{
                        x,
                        y,
                        delay: 0,
                        height: "0px",
                        display: "none"
                    }
                })
                dispatch(fetchDataAsync(i)).then((e) => {
                    setCardState([...cardState, e.payload.image])
                    noIcon.current.style.opacity = 0
                })
            }
            return [
                {
                    x,
                    y,
                    rot,
                    scale,
                    delay: 0,
                    config: { friction: 50, tension: active ? 800 : isGone ? 200 : 500 },
                    display: "flex",
                    opacity: 1,
                },
                {
                    x,
                    y,
                    rot,
                    scale,
                    delay: 150,
                    config: { friction: 50, tension: active ? 800 : isGone ? 200 : 500 },
                    display: "none",
                    opacity: 0,
                }
            ]
        })
        //if (!active && gone.size === cards.length)
        //    setTimeout(() => {
        //        gone.clear()
        //        api.start(i => to(i))
        //    }, 600)
    })

    
    // Now we're just mapping the animated values to our view, that's it. Btw, this component only renders once. :-)
    return (
        <>
        {springs.map(({ x, y, rot, scale, display, opacity  }, i) => {
            
            console.log(opacity)
            return (
                <>
                <animated.div className={styles.deck} key={i} style={{ x, y, display,
                    alignItems: "flex-end"}}>
                {/* This is the card itself, we're binding our gesture to it (and inject its index so we know which is which) */}
                
                <animated.div
                    {...bind(i)}
                    style={{
                        transform: interpolate([rot, scale], trans),
                        backgroundImage: `url(${cardState[i]})`,
                        display
                    }}
                />
                    <animated.div {...bind(i)} style={{
                        background:"#fff",
                        opacity:0.5,
                        position:"absolute",
                        height:"auto",
                        transform: interpolate([rot, scale], trans),
                    }}>
                        <h1>Product name</h1>
                        <p>Description....</p>
                    </animated.div>
                    
                </animated.div>
                
                </>
            )}
        )}
        {springsDetail.map(({ x, y, height, display}, i) => {
                    return (
                        <animated.div key={i} ref={detailRef} style={{
                            display:display,
                            color: "red",
                            height: height
                        }}>
                            <div style={{
                                background: `url(${cardState[i]})`,
                                backgroundSize: "cover",
                                height: "100vh",
                                
                            }}>
                                <div style={{
                                    backgroundColor:"rgba(255,255,255,0.8)",
                                    height: "100vh",
                                    color:'black',
                                    textAlign:"left",
                                    paddingLeft:"10px",
                                    paddingRight:"10px",
                                }}>
                                    <h2>{crds.activeCard? crds.activeCard.title:"title"}</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam laoreet molestie rhoncus. Nunc consectetur egestas faucibus. Morbi placerat, elit id suscipit finibus, sapien justo dignissim nibh, in facilisis sem mi sed quam. Quisque vel risus eleifend, dignissim risus sed, congue felis. Aliquam facilisis ac felis id gravida. Morbi id dui in ex viverra iaculis ut id nisi. Nulla et nulla quis sapien efficitur rhoncus. Donec quis ex et tellus tincidunt dictum. Etiam pulvinar ex quis tortor mollis accumsan. Nam accumsan mauris sed dolor consectetur imperdiet. Duis euismod fermentum dignissim.</p>
                                    <Galery images={crds.activeCard?crds.activeCard.galery:[]}/>
                                    <h2>200 KČ</h2>
                                </div>
                            </div>
                        </animated.div>
                    )
                })}
        </>
    )
}

    export default function App() {
        const okIcon = useRef()
        const noIcon = useRef()
        const dispatch = useDispatch();
        const {user} = useSelector(selectCards);

    const getContent = () => {
        console.log(user)
        if (true){
            return (
                <div className={`flex fill center ${styles.container}`}>
            <Deck okIcon={okIcon} noIcon={noIcon} />
            <div className={styles.signal}>
                <CheckCircleOutlined className={styles.okIcon} ref={okIcon} style={{
                    opacity: 0,
                    pointerEvents:"none",    
                }}/>
                <CloseCircleOutlined className={styles.noIcon} ref={noIcon} style={{
                    opacity: 0,
                    pointerEvents:"none",  
                }}  />
            </div>
        </div>
            )
        }

        return "Page restricted"
    }
    return (
        <>
            {getContent()}
        </>
    )
}
