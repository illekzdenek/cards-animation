import React, {useEffect} from 'react'

export default function useWindowSize() {
    /* For server-side rendering */
    //const isSSR = typeof window !== "undefined";
    //const [windowSize, setWindowSize] = React.useState({
    //    width: isSSR ? 1200 : window.innerWidth,
    //    height: isSSR ? 800 : window.innerHeight,
    //});

    const [windowSize, setWindowSize] = React.useState({
        width: 0,
        height: 0
    });

    useEffect(() => {
        window.addEventListener("resize", () => {
            setWindowSize({ width: window.innerWidth, height: window.innerHeight });
        });
    
        return () => {
            window.removeEventListener("resize", () => {
                setWindowSize({ width: window.innerWidth, height: window.innerHeight });
            });
        };
    }, []);

    
    return windowSize;
}
