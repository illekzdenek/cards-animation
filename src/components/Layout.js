import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import {
    setView,
    selectCards,
} from '../reducers/cardsReducer';
import * as View from "../constants/view"
import LoginForm from './LoginForm';

import Cards from "./Cards"
import { Layout, Menu, Breadcrumb } from 'antd';

const { Header, Content, Footer } = Layout;


export default function LayoutMain() {

    const dispatch = useDispatch();
    const {view} = useSelector(selectCards);
    console.log(view)

    const getMenu = () => {
        return (
            <Header style={{
                position:"absolute",
                bottom:"0",
                width:"100vw",
                paddingLeft:0,
                paddingRight:0
                }}>
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['0']} style={{
                }}>
                    <Menu.Item onClick={() => dispatch(setView(""))} key={0}>{`nav ${0}`}</Menu.Item>
                    <Menu.Item onClick={() => dispatch(setView(View.CARDS))} key={1}>{`nav ${1}`}</Menu.Item>
                    <Menu.Item onClick={() => dispatch(setView(View.SETTINGS))} key={2}>{`nav ${2}`}</Menu.Item>
                </Menu>
            </Header>
        )
    }

    const getLayout = () => {
        switch (view) {
            case View.CARDS:
                return (
                    <>
                        <Cards/>
                        {getMenu()}
                    </>
                );
            case View.SETTINGS:
                return (
                    <>
                        <h1>Settings</h1>
                        
                        {getMenu()}
                    </>
                );
        
            default:
                return (
                    <>
                        <div style={{
                            paddingLeft: "10px",
                            paddingRight: "10px"
                        }}>
                            <LoginForm/>
                        </div>
                        {getMenu()}
                    </>
                );
        }
        
    }
    

    return (
        <div style={{
            overflowX:"hidden"
        }}>
            {getLayout()}
        </div>
    )
}
