import React, {useState} from 'react'
import { Form, Input, Button, Checkbox } from 'antd';
import { postLogin, postRegister} from "../api/serverApi"

import { useSelector, useDispatch } from 'react-redux';
import {
    login,
    setView,
    selectCards,
} from '../reducers/cardsReducer';
import * as View from "../constants/view"

export default function LoginForm() {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const dispatch = useDispatch();
    const {view} = useSelector(selectCards);

    async function registerFetch(){
        postRegister(email, password)
    }
    
    async function loginFetch(){
        if (email === "zdenda" && password === "zdenda"){
            dispatch(setView(View.CARDS))
        }
        postLogin(email,password,(data) => {
            console.log(typeof data)
            dispatch(login(data))
            dispatch(setView(View.CARDS))
        })
        
    }

    const onFinish = (values) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <>
            <div style={{
                backgroundImage: "url(/logo.png)",
                backgroundSize:"cover",
                backgroundPosition:"center",
                width: "100vw",
                height:"240px"
            }}></div>
            <Form
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="Username"
                    name="username"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your username!',
                        },
                    ]}
                >
                    <Input onChange={(e) => setEmail(e.target.value)} value={email}/>
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                    ]}
                >
                    <Input.Password onChange={(e) => setPassword(e.target.value)} value={password}/>
                </Form.Item>

                <Form.Item
                >
                    <Button type="primary" htmlType="submit" onClick={registerFetch}>
                        Register
                    </Button>
                    <Button type="primary" htmlType="submit" onClick={loginFetch}>
                        Login
                    </Button>
                </Form.Item>
            </Form>
        </>
    )
}
