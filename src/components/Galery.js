
import React, { useRef, useState } from 'react'
import { Parallax, ParallaxLayer } from '@react-spring/parallax'
import styles from './styles.module.css'

import {Button, Modal} from "antd"
import {SearchOutlined} from "@ant-design/icons"


export default function Galery(props) {

    const [modalOpened, setModalOpened] = useState(false)
    const [modalImage, setModalImage] = useState("")


    const parallax = useRef(null)

    const scroll = (to) => {
        if (parallax.current) {
            parallax.current.scrollTo(to)
        }
    }

    const onClickButton = (offset) => {
        console.log("onClickButton")
        setModalOpened(!modalOpened)
        setModalImage(props.images[offset])
        console.log(props.images)
        console.log(offset)
    }

    const getImages = () => {
        console.log(props)
        let imagesElements = []
        for (let [index, i] of props.images.entries()){
            const next = index === props.images.length-1? 0: index+1
            imagesElements.push(
                <Page offset={index} gradient="pink" onClick={() => scroll(next)} image={i} onClickButton={(offset)=>onClickButton(offset)}/>
            )
        }
        return (
            imagesElements
        )
    }

    return (
        <>
            <Modal visible={modalOpened} onCancel={() => setModalOpened(false)}>
                <img src={modalImage} style={{
                    width:"100%"
                }}/>
            </Modal>
            <div style={{ background: '#dfdfdf' }}>
                <Parallax className={styles.containerParalax} ref={parallax} pages={props.images.length} horizontal style={{
                    position:"block",
                    width:"100%",
                    height:"500px"
                }}>
                
                    {getImages()}
                </Parallax>
            </div>
        </>
    )
}


const Page = ({ offset, gradient, onClick, image, onClickButton }) => (
    <>
        <ParallaxLayer offset={offset} speed={0.2} onClick={onClick}>
            <div className={styles.slopeBegin} style={{
                backgroundImage: `url(${image})`,
                backgroundSize: "cover",
                backgroundPosition: "center"
            }} />
        </ParallaxLayer>

        <ParallaxLayer offset={offset} speed={0.6} onClick={onClick}>
            <div className={`${styles.slopeEnd} ${styles[gradient]}`} />
        </ParallaxLayer>

        <ParallaxLayer className={`${styles.text} ${styles.number}`} offset={offset} speed={0.3}>
            {/* <span>0{offset + 1}</span> */}
            <Button onClick={()=>onClickButton(offset)} type="ghost" size='large' shape="circle" icon={<SearchOutlined style={{
                color:"#fff"
            }} />} style={{
                position:"absolute",
                left:"50%",
                transform:"translate(-50%, -50%)",
                width:"60px",
                height:"60px",
                pointerEvents:"initial",
                border: "3px solid white"
            }}/>
        </ParallaxLayer>
    </>
)
