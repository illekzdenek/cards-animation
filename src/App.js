import React from 'react';
import useWindowSize from './components/utils/useWindowSize';
import LayoutMain from './components/Layout';
import './App.css';
import 'antd/dist/antd.css';


function App() {
  const windowSize = useWindowSize();

  const getContent = () => {
    // if (windowSize.width > 425){
    //   //console.log(windowSize)
    //   return(
    //     <h1>Only phone width is allowed.Turn window width to 425px or lower.</h1>
    //   )
    // }

    return(
      <div className="App">
        <LayoutMain/>
        
      </div>
    )
  }

  return getContent()
}

export default App;
