import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../reducers/counterReducer';
import cardsReducer from '../reducers/cardsReducer';

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    cards: cardsReducer
  },
});
